import React, { Component } from 'react'
import RegisterForm from './RegisterForm';




export default class LoginForm extends Component {


    state = {
        username: '',
        password: '',
        buttonDisabled: 'false',
        register: false
    }

    onChange(e) {
        var val = e.target.value.trim();
        if (val.length > 12) return
        this.setState({
            [e.target.name]: val
        })
    }

    resetForm() {
        this.setState({
            username: '',
            password: '',
            buttonDisabled: 'false',
        })
    }
    register() {
        this.setState({
            register: !this.state.register
        })
    }

    async doLogin() {

        if (!this.state.username || !this.state.password) return

        this.setState({
            buttonDisabled: 'true'
        })
        await this.props.doLogin(this.state);

        this.resetForm();
    }

    render() {
        if (!this.state.register) {
            return (
                <div className='app'>
                    <div className='container'>
                        <div className='loginForm'>
                            <div>Log in</div>
                            <div className='inputField'>
                                <input
                                    className='input'
                                    type='text'
                                    placeholder='Username'
                                    name='username'
                                    value={this.state.username ? this.state.username : ''}
                                    onChange={this.onChange.bind(this)}>
                                </input>
                            </div>
                            <div className='inputField'>
                                <input
                                    className='input'
                                    type='password'
                                    placeholder='Password'
                                    name='password'
                                    value={this.state.password ? this.state.password : ''}
                                    onChange={this.onChange.bind(this)}>
                                </input>
                            </div>
                            <div className='submitButton'>
                                <button
                                    className='btn'
                                    disable={this.state.buttonDisabled}
                                    onClick={this.doLogin.bind(this)}>
                                    Login
                                    </button>
                            </div>
                            <div className='submitButton'>
                                <button
                                    className='btn'
                                    disable={this.state.buttonDisabled}
                                    onClick={this.register.bind(this)}>
                                    Create Account
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div className='app'>
                    <div className='container'>
                        <RegisterForm doRegister={this.props.doRegister} register={this.register} />
                        <div className='submitButton'>
                            <button
                                className='btn'
                                disable={this.state.buttonDisabled}
                                onClick={this.register.bind(this)}>
                                Have Account? Log in
                            </button>
                        </div>
                    </div>
                </div>
            )
        }

    }
}
