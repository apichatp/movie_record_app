// import { observer } from 'mobx-react-lite';
import React from 'react';
import '../App.css';

class Header extends React.Component {

    render() {
        return (
            <header className='App-header'>
                <div>Movie Records ({this.props.role})</div>
                <button 
                    className='headerbtn' 
                    onClick={this.props.handleClick.bind(this,'logout')}>
                        Log out
                    </button>
            </header>
        );
    }
}

export default Header;