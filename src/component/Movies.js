import React, { Component } from 'react'
import MovieItem from './MovieItem';

export default class Movies extends Component {
    render() {
        return this.props.movies.map((movie) => (
            <MovieItem 
                movie={movie}
                role={this.props.role} 
                onDelete={this.props.onDelete} 
                onUpdate={this.props.onUpdate} />
        ))
        
           
        
    }
}
