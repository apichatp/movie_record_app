import React from "react";
// import { observable} from 'mobx';
import '../App.css';

class AddMovie extends React.Component {

    state = {
        title: '',
        year: '',
        rating: ''

    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onSubmit(e) {
        e.preventDefault();
        if (!this.state.title || !this.state.year || !this.state.rating) return
        this.props.onSubmit(this.state);
        this.props.handleClick('add');
    }

    render() {
        const year = new Date().getFullYear()
        const ratings = ['G', 'PG', 'M', 'MA', 'R']
        var years = Array.from(new Array(50), (val, index) => year - index)
        return (
            <div className='add_form'>
                <form className='form' onSubmit={this.onSubmit.bind(this)}>
                    <input
                        type="text"
                        name='title'
                        className='form-input'
                        placeholder="Movie Title"
                        value={this.state.title}
                        onChange={this.onChange.bind(this)}
                    />

                    <select name='year' className='form-input' value={this.state.year} onChange={this.onChange.bind(this)}>
                        <option value="">Year</option>
                        {years.map((year) => <option value={year}>{year}</option>)}
                    </select>

                    <select name='rating' className='form-input' value={this.state.rating} onChange={this.onChange.bind(this)}>
                        <option value="">Rating</option>
                        {ratings.map((rating) => <option value={rating}>{rating}</option>)}
                    </select>

                    <input
                    onClick={this.props.handleClick.bind(this,'add')}
                        type='submit'
                        value='Cancel'
                        className='btn-add'
                        style={{ flex: '1' }}
                    />
                    <input
                        type='submit'
                        value='Submit'
                        className='btn-add'
                        style={{ flex: '1' }}
                    />
                </form>
            </div>
        )
    }
}


export default AddMovie;

