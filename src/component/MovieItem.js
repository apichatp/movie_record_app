import React, { Component } from 'react'
import '../App.css'
import { AiFillTool, AiFillMinusCircle, AiFillCloseCircle,AiFillCheckCircle } from 'react-icons/ai'

export default class MovieItem extends Component {

    state = {
        edit: false,
        id: '',
        title: '',
        rating: '',
        year: ''
    }

    onClick() {
        this.setState({
            edit: !this.state.edit
        })

        this.reset()
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    reset() {
        this.setState({
            id: this.props.movie.id,
            title: this.props.movie.title,
            rating: this.props.movie.rating,
            year: this.props.movie.year
        })
    }

    onEdit() {
        if (!this.state.title || !this.state.year || !this.state.rating) return



        if (this.state.title === this.props.movie.title &&
            this.state.year.toString() === this.props.movie.year.toString() &&
            this.state.rating === this.props.movie.rating){
                this.onClick();
                return
            } 
 
            let movie ={
                id: this.state.id,
                title: this.state.title,
                year: this.state.year,
                rating: this.state.rating
            }

        this.props.onUpdate(movie)
        this.onClick();
        
    }

    render() {
        if (!this.state.edit) {
                return (
                    <div className='mlist_item'>
                        <div className='label'>
                            <label>
                                Title : {this.props.movie.title}
                            </label>
                            <label>
                                Year : {this.props.movie.year}

                                {this.props.role === 'Manager' ? <div 
                                    className='delete-btn' 
                                    onClick={this.props.onDelete.bind(this, this.props.movie.id)}>
                                        <AiFillCloseCircle />
                                </div> : null}

                                {this.props.role !== 'Floorstaff' ? 
                                <div 
                                    className='delete-btn' 
                                    onClick={this.onClick.bind(this)}>
                                        <AiFillTool />
                                </div> : null}
                            </label>
                            <label>
                                Rating : {this.props.movie.rating}
                            </label>
                        </div>
                    </div>
                )
        } else {
            const year = new Date().getFullYear()
            const ratings = ['G', 'PG', 'M', 'MA', 'R']
            var years = Array.from(new Array(50), (val, index) => year - index)
            return (
                <div className='mlist_item'>
                    <input
                        type="text"
                        name='title'
                        className='list-input'
                        placeholder="Movie Title"
                        value={this.state.title}
                        onChange={this.onChange.bind(this)}>
                    </input>
                    <div>
                        <select
                            name='year'
                            className='list-input'
                            value={this.state.year}
                            onChange={this.onChange.bind(this)}>
                            <option value="">Year</option>
                            {years.map((year) => <option value={year}>{year}</option>)}
                        </select>
                        <div
                            className='delete-btn'
                            onClick={this.onClick.bind(this)}>
                            <AiFillMinusCircle />
                        </div>

                        <div
                            className='delete-btn'
                            onClick={this.onEdit.bind(this)}>
                            <AiFillCheckCircle />
                        </div>
                    </div>

                    <select
                        name='rating'
                        className='list-input'
                        value={this.state.rating}
                        onChange={this.onChange.bind(this)}>
                        <option value="">Rating</option>
                        {ratings.map((rating) => <option value={rating}>{rating}</option>)}
                    </select>
                </div>
            )
        }

    }
}
