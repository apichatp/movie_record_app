import React, { Component } from 'react'
import '../App.css'

export default class RegisterForm extends Component {

    state = {
        username: '',
        password: '',
        role: '',
        buttonDisabled: 'false',
    }

    resetForm() {
        this.setState({
            username: '',
            password: '',
            role: '',
            buttonDisabled: 'false',
        })
    }

    onChange(e) {
        var val = e.target.value.trim();
        if (val.length > 12) return
        this.setState({
            [e.target.name]: val
        })
    }

    async doRegister() {
        if (!this.state.username || !this.state.password || !this.state.role) return
        this.props.doRegister(this.state);
        this.resetForm();
    }

    render() {
        const roles = ['Manager', 'Teamleader', 'Floorstaff']
        return (
            <div className='loginForm'>
                <div>Register</div>
                <div className='inputField'>
                    <input
                        className='input'
                        type='text'
                        placeholder='Username'
                        name='username'
                        value={this.state.username ? this.state.username : ''}
                        onChange={this.onChange.bind(this)}>
                    </input>
                </div>
                <div className='inputField'>
                    <input
                        className='input'
                        type='password'
                        placeholder='Password'
                        name='password'
                        value={this.state.password ? this.state.password : ''}
                        onChange={this.onChange.bind(this)}>
                    </input>
                </div>
                <div className='inputField'>
                    <select
                        className='input'
                        name='role'
                        value={this.state.role}
                        onChange={this.onChange.bind(this)}>
                        <option value="">Role</option>
                        {roles.map((role) => <option value={role}>{role}</option>)}
                    </select>
                </div>
                <div className='submitButton'>
                    <button
                        className='btn'
                        disable={this.state.buttonDisabled}
                        onClick={this.doRegister.bind(this)}>
                            Register
                        </button>
                </div>
            </div>
        )
    }
}
