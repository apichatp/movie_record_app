
const serverPath = 'http://localhost:5000'
const api = {
    login: '/login',
    register: '/register',
    addMovie: '/addMovie',
    deleteMovie: '/deleteMovie',
    getMovie: '/getMovie',
    updateMovie:'/updateMovie',
    isLoggedIn:'/isLoggedIn',
}

module.exports = {
    serverPath,api
}