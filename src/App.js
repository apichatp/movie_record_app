import { observer } from 'mobx-react';
// import { observer } from 'mobx-react-lite';
import React from 'react';
import './App.css';
import AddMovie from './component/AddMovie';
import Movies from './component/Movies';
import Header from './component/Header';
import movieStore from './stores/MovieStore';
import userStore from './stores/UserStore';
import LoginForm from './component/LoginForm';



class App extends React.Component {

  movieData = movieStore.getMovies()
  userData = userStore.getUser()
  
  handleClick = (e) => {
    if (this.userData.isLoggedIn) {
      userStore.changeButton(e)
    }
  }

  async addMovie(value) {
    userStore.changeButton('loading')
    let message =  await movieStore.addMovieData(value);
    await movieStore.getMovieData();
    alert(message)
    userStore.changeButton('loading')
  }

  async deleteMovie(value){
    userStore.changeButton('loading')
    let message = await movieStore.deleteMovieData(value);
    await movieStore.getMovieData();
    alert(message)
    userStore.changeButton('loading')
  }

  async updateMovie(value){
    userStore.changeButton('loading')
    let message = await movieStore.updateMovieData(value);
    await movieStore.getMovieData();
    alert(message)
    userStore.changeButton('loading')
  }

  doLogin(val) {
    userStore.changeButton('loading')
    userStore.Login(val)
    movieStore.getMovieData();
    userStore.changeButton('loading')
  }

  doRegister(val) {
    userStore.changeButton('loading')
    userStore.Register(val)
    movieStore.getMovieData();
    userStore.changeButton('loading')
  }

  render() {
    if (this.userData.loading) {
      return (
        <div className='app'>
          <div className='container'>
            Loading, please wait.....
          </div>
        </div>
      )
    } else {
      if (this.userData.isLoggedIn) {
        if (this.movieData.length > 0) {
          return (
            <div className='app'>
              <Header handleClick={this.handleClick} btn={this.userData.add} role={this.userData.role} />
              <div className='main'>
              {
                  this.userData.add ? <AddMovie handleClick={this.handleClick} onSubmit={this.addMovie} /> :
                    <div>
                      <button
                        className='btn-add'
                        onClick={this.handleClick.bind(this,'add')}>
                        Add
                      </button>
                    </div>
                  }
                <div>
                  <Movies 
                    movies={this.movieData} 
                    role={this.userData.role} 
                    onDelete={this.deleteMovie}
                    onUpdate={this.updateMovie} />
                </div>
              </div>
            </div>
          )
        } else {
          return (
              <div className='app'>
                <Header handleClick={this.handleClick} btn={this.userData.add} />
                <div className='main'>
                {
                    this.userData.add ? <AddMovie handleClick={this.handleClick} onSubmit={this.addMovie} /> :
                      <div>
                        <button
                          className='btn-add'
                          onClick={this.handleClick.bind(this,'add')}>
                          Add
                        </button>
                      </div>
                    }
                  <div>
                    <Movies 
                      movies={this.movieData} 
                      role={this.userData.role} 
                      onDelete={this.deleteMovie}
                      onUpdate={this.updateMovie} />
                  </div>
                </div>
              </div>
            )
        }
      } else {
        return (
          <LoginForm 
            doLogin={this.doLogin} 
            onClick={userStore.changeButton} 
            doRegister={this.doRegister} />
        )
      }
    }
  }

}

export default observer(App);
