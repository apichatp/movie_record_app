// import { get } from 'mobx';
import { types, getParent, destroy } from 'mobx-state-tree';
import { serverPath, api } from '../servicePath';

const user = types
    .model("user", {
        loading: types.boolean,
        add: types.boolean,
        isLoggedIn: types.boolean,
        id: types.string,
        username: types.string,
        role: types.string
    }).actions(self => ({
        remove() {
            getParent(self, 2).remove(self)
        },
    }))

const userStore = types
    .model({
        userData: types.optional(user, {
            loading: false,
            add: false,
            register: false,
            isLoggedIn: false,
            id: '',
            username: '',
            role: ''
        })
    }
    ,
    )
    .actions(self => ({
        add(item) {
            self.userData = {
                loading: false,
                add: false,
                isLoggedIn: item.username ? true : false,
                register: false,
                id: item.id ? item.id.toString() : '',
                username: item.username ? item.username : '',
                role: item.role ? item.role : ''
            }
        },
        remove(item) {
            destroy(item);
        },
        changeButton(e) {
            switch (e) {
                case ('loading'):
                    self.userData.loading = !self.userData.loading
                    break;

                case ('add'):
                    self.userData.add = !self.userData.add
                    break;
                case ('register'):
                    self.userData.register = !self.userData.register
                    break;
                case ('logout'):
                    this.add({
                        loading: false,
                        add: false,
                        register: false,
                        isLoggedIn: false,
                        id: '',
                        username: '',
                        role: ''
                    })
                    break;
                default:
                    break;
            }
        },
        async Login(data) {
            let authorization = data.username + ":" + data.password
            let encode = Buffer.from(authorization).toString('base64')
            try {
                let res = await fetch(serverPath + api.login, {
                    method: 'get',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                        'authorization': "Basic " + encode
                    },
                })
                let result = await res.json();
                if (result && result.success) {
                    this.add(result)
                    return self.userData;
                } else if (result && result.success === false) {
                    alert(result.message)
                    return false
                }
            } catch (e) {
                console.log(e)
                return false
            }
        },
        async Register(data) {
            let authorization = data.username + ":" + data.password
            let encode = Buffer.from(authorization).toString('base64')
            try {
                let res = await fetch(serverPath + api.register, {
                    method: 'post',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                        'authorization': "Basic " + encode
                    },
                    body : JSON.stringify({
                        role:data.role
                    }) ,
                })
                let result = await res.json();
                if (result && result.success) {
                    this.add(result)
                    return self.userData;
                } else if (result && result.success === false) {
                    alert(result.message)
                    return false
                }
            } catch (e) {
                console.log(e)
                return false
            }
        }
    }))
    .views(self => ({
        getUser() {
            return self.userData
        }
    }))

const store = userStore.create()


export default store