// import { get } from 'mobx';
import { types, getParent, destroy } from 'mobx-state-tree';
import { serverPath, api } from '../servicePath';

const movie = types
    .model("movie", {
        id: types.number,
        title: types.string,
        year: types.number,
        rating: types.string
    }).actions(self => ({
        remove() {
            getParent(self, 2).remove(self)
        },
    }))

const movieStore = types
    .model({
        movies: types.optional(types.array(movie), []),
    })
    .actions(self => ({
        reset(){
            self.movies = []
        },
        addMovie(movie) {
            self.movies.push(movie);
        },
        remove(item) {
            destroy(item);
        },
        changeFilter(filterName) {
            self.filter = filterName
        },
        async getMovieData() {
            try {
                let res = await fetch(serverPath + api.getMovie, {
                    method: 'get',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                    },
                })
                let result = await res.json();
                if (result && result.success) {
                    this.reset();
                    await result.movies.forEach(movie => {
                        this.addMovie({
                            id: movie.id,
                            title: movie.title,
                            year: movie.year,
                            rating: movie.rating
                        })
                    }) 
                }
            } catch (e) {
                console.log(e)
                return false
            }
        },
        async addMovieData(val) {
            try {
                let res = await fetch(serverPath + api.addMovie, {
                    method: 'post',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                    },body: JSON.stringify({
                        title:val.title,
                        year:val.year,
                        rating:val.rating
                    })
                })
                let result = await res.json();
                if (result.success) {
                    // alert('Add Completed')
                    return 'เพิ่มข้อมูลเสร็จเรียบร้อย'
                }
            } catch (e) {
                console.log(e)
                return e.message
            }
        },
        async deleteMovieData(val) {
            try {
                let res = await fetch(serverPath + api.deleteMovie, {
                    method: 'post',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                    },body: JSON.stringify({
                        id:val,
                    })
                })
                let result = await res.json();
                if (result.success) {
                    // alert('Delete Compete')
                    return 'ลบข้อมูลเสร็จเรียบร้อย'
                }else {
                    // alert(result.message)
                    return result.message
                }
            } catch (e) {
                console.log(e)
                return e.message
            }
        },
        async updateMovieData(val) {
            try {
                let res = await fetch(serverPath + api.updateMovie, {
                    method: 'post',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Access-Control-Allow-Origin': '*',
                    },body: JSON.stringify({
                        id:val.id,
                        title:val.title,
                        year:val.year,
                        rating:val.rating
                    })
                })
                let result = await res.json();
                if (result.success) {
                    // alert('Update Completed')
                    return 'แก้ไขข้อมูลเสร็จเรียบร้อย'
                }else {
                    return result.message
                }
            } catch (e) {
                console.log(e)
                return e.message
            }
        },
    }))
    .views(self => ({
        getMovies() {
            return self.movies
        }
    }))

const store = movieStore.create()


export default store